---
title: "Suivi De Projet"
date:  2018-07-04T17:14:16+02:00
weight: 5
---

## Introduction

Nous utilisons Gitlab.com pour le suivi de nos projets. **Tout y est public** ; tout un chacun peut tout lire.

Pour participer aux échanges il convient de suivre les [3 étapes pour créer son compte](/groupes-de-travail/informatique/gitlab/)

## Liens utiles pour le suivi des projets et tâches

1. Les [Projets en cours](https://gitlab.com/Glenans-CDMEB/Projets/issues?label_name%5B%5D=Projet)
2. [Toutes les tâches](https://gitlab.com/Glenans-CDMEB/Projets/issues?scope=all&utf8=%E2%9C%93&state=opened) (projets inclus)
3. [Tâches où de l'aide est souhaitée](https://gitlab.com/Glenans-CDMEB/Projets/issues?label_name%5B%5D=Aide+souhait%C3%A9e)

## Un peu de vocabulaire

Pour Gitlab, tout est *issue*. Une *issue* correspond à une tâche à faire, indépendamment de sa taille. Nous (CDMEB) avons
décidé de qualifier de "projet" une tâche importante composée de plusieurs *issues*.

## Calendrier et concept de *milestone*

Pour poser les projets et tâches sur le calendrier, nous utilisons les *milestones* (ou jalons). Nous (CDMEB) avons
décidé de poser une milestone par trimestre, par exemple 2018-Q1 pour le premier trimestre 2018 (cette échelle de temps
correspond assez bien au tempo du bénévolat).

Il est possible d'affecter une *milestone* à une *issue*, et la [vue des *milestones*](https://gitlab.com/Glenans-CDMEB/Projets/milestones), permet de voir l'avancement par trimestre. Il est aussi possible de cliquer sur une *milestone* pour voir graphiquement comment le travail avance (le grahique s'appelle un "*burn down chart*").