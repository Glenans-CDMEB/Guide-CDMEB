---
title: "Recommandations nommage de fichier"
description: ""
---

Voici une liste de recommandation pour nommer les fichiers à partager sur l'espace **documentation moniteurs**.

## Recommandations de nommage

* Première lettre en majuscule, les autres en minuscules (ne pas écrire tout en majuscule c'est plus compliqué à lire)
* Ajouter dans le nom la date au format YYYY-mm-dd (par exemple 2017-09-20)
* Ajouter si possible un numéro de version (par défaut `v1.0`)
* Éviter les accents (pas pratique pour les liens hypertextes)
* ~~Remplacer les espaces par des underscore `_`~~

## Exemples

Document sur l'encadrement de stage dériveur

*  sans recommandations : `ENCADRER UN STAGE DERIVEUR DEBUTANT Version finale LO avec images.odt`
*  avec recommandations : `Encadrer un stage deriveur debutant 2017-09-20 v1.0.odt`