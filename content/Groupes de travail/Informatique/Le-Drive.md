---
title: "Le Drive"
date:  2018-01-07T21:11:12+01:00
weight: 4
---

Le *Drive* est un disque dur partagé, sécurisé et accessible par internet. Il nous évite d’envoyer les fichiers en pièce jointe et allège ainsi nos échanges de mail. Au lieu de placer le fichier dans le courriel, on le place sur le *Drive* et on communique son emplacement sur le *Drive*.

##  Accès au Drive

###  Adresse de connexion

[https://drive.cdmeb.org/](https://drive.cdmeb.org/)

### Identifiant

Première lettre du prénom suivi du nom, le tout en minuscule sans point ni espace. Exemple pour Pierre-Yves Durand : `pydurand`.

### Mot de passe

Lors de votre première connexion (ou si vous avez oublié votre mot de passe), saisissez votre identifiant puis `aaaa` en mot de passe.

Vous obtenez un message d’erreur indiquant un mot de passe erroné. C’est normal, cliquez sur le lien « Ré-initialiser le mot de passe » et suivez les instructions.

## Organisation des fichiers

Il existe un dossier par année (par exemple `Dossier-2017`, ou encore `Dossier-2018`) dans lesquels sont rangés les projets de chaque année. Il existe aussi un `Dossier-Permanent` pour les documents intemporels, comme les statuts de l’association ou la charte du CDMEB.

Le rangement des documents sur le Drive est opéré par l’équipe Documentalistes, joignable à `technique AROBASE cdmeb.org`.

## Ajouter un fichier

Pour ajouter un fichier il suffit de :

1. Placer le fichier sur le *Drive* dans le dossier `CDMEB_upload`.
2. Envoyer un courriel à `technique AROBASE cdmeb.org`, en indiquant où il devrait être rangé.
3. Un membre de l’équipe technique vous renvoie un courriel une fois le document rangé.

## Accès par application native (utilisateurs avancés)

Pour les utilisateurs avancés, il est possible d'utiliser le Drive sans passer par son navigateur, ce qui peut s'avérer pratique (notamment pour envoyer plusieurs fichiers).

* [Applications NextCloud pour ordinateurs et mobiles](https://nextcloud.com/install/#install-clients) (Note Matthieu: fonctionne très bien)
* [Accès par protocole WebDav](https://docs.nextcloud.com/server/12/user_manual/files/access_webdav.html) (l'URL à utiliser est `drive.cdmeb.org/remote.php/dav/files/MON-IDENTIFIANT` en remplaçant MON-IDENTIFIANT par son identifiant, par exemple `pydurand` pour Pierre-Yves Durand)

**Attention, warning, achtung !** Les utilisateurs ayant les droits d'écriture doivent être particulièrement vigilants en utilisant ces techniques : une suppression de fichier sur son ordinateur entraine *instantanément* la suppression sur le serveur !
