---
title: "Entrée / sortie de membre CDMEB"
description: ""
---

Le but est de lister les tâches à effectuer pour les entrées / sorties de membres du CDMEB

## Dresser les listes des membres

* [ ]  Faire la liste des entrants année N
* [ ]  Faire la liste des sortants année N-1 donc
* [ ]  Faire la liste complète des membres de l'année N
    * membres élus
    * membres associés
* [ ]  Mettre à jour la liste de coordonnées de membres (tableur sur le Drive, dossier `Année N > 2 - Liste des membres`

## Membres élus / associés

Les membres associés n'ont pas de droit de vote. Ils sont cependant les bienvenus à participer aux réunions et  groupes
de travail du CDMEB.   

## Pour les membres SORTANTS

### Drive

* [ ] DRIVE : Créer le groupe "CDMEB 20xx anciens membres"
* [ ] DRIVE : Enlever le compte de la personne du groupe "CDMEB"
* [ ] DRIVE : Ajouter le compte de la personne dans le groupe "CDMEB 20xx anciens membres"

### Listes de discussion

La mise à jour des listes s'opère depuis le [Manager OVH](https://www.ovh.com/manager/) `TS222022` : Web > Emails > cdmeb.org > Mailing lists

* [ ] LISTES : Supprimer les membres sortants de la liste `membres@cdmeb.org`. 
* [ ] LISTES : Proposer aux membres sortants de les ajouter à la liste `sympathisants@cdmeb.org`

## Pour les membres ENTRANTS

### Drive

* [ ] DRIVE :Créer le compte de la personne et l'ajouter dans le groupe `CDMEB` 

### Listes de discussion

La mise à jour des listes s'opère depuis le [Manager OVH](https://www.ovh.com/manager/) `TS222022` : Web > Emails > cdmeb.org > Mailing lists

* [ ] LISTES : Ajouter sur la liste `membres@cdmeb.org` les membres élus et associés

### Site web

* [ ] WEB : Créer la page web des membres 2020 + mise à jour du lien
* [ ] WEB : Afficher calendrier de nos réunions + contact

### Actions de bienvenue

* Envoyer le mail de bienvenu sur la liste membres. 
* Sujet : Bienvenue au nouveaux arrivants CDMEB
* Contenu : ci-dessous

>>>
Bonjour,

Souhaitons la bienvenue sur la liste membres aux nouveaux arrivants au CDMEB !

* Prénom NOM

Voici la [liste des membres 2020 (élus et associés)](). Je vous invite à vérifier vos coordonnées. 
Cette liste contenant des données personnelles, elle est distribuée dans les conditions suivantes :

* la liste est destinée aux communication un-à-un et un-à-plusieurs ;
* elle ne doit *pas* être utilisée pour spammer toutes les adresses ;
* elle ne doit *pas* être utilisée pour abonner les adresses à d’autres listes de diffusion ;
* elle ne doit *pas* être diffusée hors du CDMEB.

Voici le [Livret d'accueil des outils du CDMEB (format PDF, 1.8Mo)](https://drive.cdmeb.org/index.php/s/e8ppxeBo4JNw4LW). En résumé, vous avez un compte sur le Drive, et au gré des groupes de travail, vous serez peut-être amenés à travailler sur le Gitlab.

L’aspect formel ayant été posé, merci à vous de nous avoir rejoint au CDMEB, et bienvenue à bord ;)

Matthieu
>>>
