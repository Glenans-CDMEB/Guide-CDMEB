---
title: "Cycle De Vie d'un Document"
date:  2018-01-07T15:53:41+01:00
weight: 5
---

Voici les étapes :

1. Un groupe (disons les MDM) travaille et place ses fichiers dans son espace de stockage sur le Drive (ici dans le dossier `CDMEB/Dossier-2018/Groupes de travail/MDM`, qui n'est accessible qu'aux membres du CDMEB)
2. Une fois le document terminé et prêt à diffuser, il est transmis à l'équipe Documentaliste qui le range dans le Drive, mais dans la section publique joignable avec les identifiants/mot de passe `visiteur/Glenans1947`, qui est l'entrepôt de données de la Doc moniteurs. Dans l'exemple MDM, il y a de fortes chances pour que le document soit placé dans `DOC_moniteurs/06_Vie_maritime/Thermostat_6_Beaufort`.
3. Enfin, une fiche de lecture est créée pour être publiée sur le site [DOC.moniteurs.glenans.asso.fr](https://DOC.moniteurs.glenans.asso.fr/)
4. La communication sur la nouvelle fiche de lecture est lancée.
