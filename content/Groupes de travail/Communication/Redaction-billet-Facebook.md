---
title: "Rédaction billet Facebook"
weight: 5
---

## Rappel

Facebook n'est utilisé que pour la *diffusion* d'un article existant sur notre site WWW.moniteurs.glenans.asso.fr (intérêt: avoir une seule et unique source d'information ; corollaire : les statistiques sont plus faciles à consolider)

Dit autrement, nous souhaitons éviter d'avoir un contenu qui n'existerait que sur un réseau social et pas sur notre site

## Rédaction du message

* la rédaction doit se faire en tant que "Glénans_CDMEB" (pas en tant que personne X ou Y)
* penser à ajouter une mention à @Les_Glenans_ecole_de_voile
* ne pas oublier de mettre le lien vers la news :)