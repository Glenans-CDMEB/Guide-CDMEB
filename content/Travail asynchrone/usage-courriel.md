---
title: "Les bonnes pratiques de l'usage des courriels"
menuTitle: "Usage du courriel"
description: ""
---

## 1. Fonctions de base

### J'utilise ``répondre`` <img class="img-inline" src="/images/Mail-reply-sender.svg"/> quand :

- Je ne souhaite parler qu'à l'expéditeur du mail
- Je lui donne une information courte sur ma présence/absence lors d'une réunion qu'il met en place
- Je lui transmet une information confidentielle que je ne veux pas transmettre à tout le monde

### J'utilise ``répondre à tous`` <img class="img-inline" src="/images/Mail-reply-all.svg"/> quand :

- Je veux parler au groupe de discussion entier
- J'ai des idées à faire passer
- J'émet une opinion sur la discussion en cours

### J'utilise ``transférer à`` <img class="img-inline" src="/images/Mail-forward.svg"/> quand :

- L'information du mail peut intéresser un de mes contacts
- Il ne s'agit pas d'une information confidentielle
- Il s'agit seulement d'une information, pas d'une discussion en cours

## 2. Utilisation "avancée"

- Je peux faire un ``répondre à tous`` et rajouter un autre destinataire (soit en destinataire, soit en copie carbone). 
	- Dans ce cas je préviens le groupe de discussion de qui est ajouté et pourquoi dans le corps du mail

## 3. Type de destinataires

- ``Destinataire`` / To : La ou les personnes à qui le mail s'adresse en premier lieu. L'information est important pour eux ou l'on attend une réponse de leur part
- ``Cc`` / ``Copie carbone`` / Carbon copy : La ou les personnes qui peuvent être intéressées par la discussion mais ne sont pas considérées comme essentielles. La discussion pourra se conclure sans leur intervention
- ``Cci`` / ``Copie Carbone Invisible`` / Blind carbon copy : **Option considérée comme malpolie ou malveillante** Son usage est déconseillé. L'usage de la copie carbone et du transfert sont à privilégier pour maintenir une communication claire. 

## 4. "Au-delà de trois courriels, on s'appelle !"

* Cette pratique évite les quiproquos, les incompréhensions et in-fine la mise sous tension entre deux personnes ou dans un groupe.
* Elle permet aussi de maintenir une communication claire entre les participants

## 5. Répondre sur une liste de discussion

* Les listes de discussion du CDMEB sont configurées de sorte que la réponse s'adresse à **tous les membres de la liste** (et pas uniquement à l'émetteur du message initial). Ceci permet de favoriser les échanges et la discussion.
* Si on souhaite répondre uniquement à l'émetteur du message initial, il convient de modifier le champs `Destinataire:` et de placer le courriel de la personne souhaitée.  

## 6. Raccourcis claviers

- [Raccourcis clavier de Gmail - Ordinateur - Aide Gmail](https://support.google.com/mail/answer/6594?co=GENIE.Platform%3DDesktop&hl=fr)
- [Raccourcis clavier pour Outlook - Outlook](https://support.office.com/fr-fr/article/Raccourcis-clavier-pour-Outlook-3cdeb221-7ae5-4c1d-8c1d-9e63216c1efd)
- [Raccourcis clavier de Thunderbird \| Assistance de Thunderbird](https://support.mozilla.org/fr/kb/raccourcis-clavier-thunderbird?redirectlocale=fr&redirectslug=raccourcis-clavier-tb)