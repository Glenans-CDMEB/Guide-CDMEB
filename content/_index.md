---
title: "Guide CDMEB"
---

# Guide du CDMEB

Le "manuel" du CDMEB (Conseil des Moniteurs et de l'Encadrement Bénévole) pour être efficace et contribuer aux actions menées.

{{% children depth="999" sort="weight" %}}